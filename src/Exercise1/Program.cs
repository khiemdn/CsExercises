﻿// ***********************************************************************
// Assembly         : Exercise1
// Author           : KhiemDN
// Created          : 09-05-2017
// Last Modified By : Khiem
// Last Modified On : 09-05-2017  10:32 AM
// ***********************************************************************
// <copyright file="Program.cs" company="VNG Corporation">
//   Copyright (c) 2017 VNG Corporation. All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************

namespace Exercise1
{
    using System;

    internal class Program
    {
        private static void Main(string[] args)
        {
            Console.WriteLine("Exercise 1 \n");
            using (Exercise1 e1 = new Exercise1("input.txt", "output.txt"))
            {
                e1.Read();
                e1.Write();
            }

            Console.ReadLine();
        }
    }
}
