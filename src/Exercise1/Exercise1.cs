﻿// ***********************************************************************
// Assembly         : Exercise1
// Author           : KhiemDN
// Created          : 09-05-2017
// Last Modified By : Khiem
// Last Modified On : 09-05-2017  2:42 PM
// ***********************************************************************
// <copyright file="Exercise1.cs" company="VNG Corporation">
//   Copyright (c) 2017 VNG Corporation. All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************

namespace Exercise1
{
    using System;
    using System.IO;

    /// <summary>
    ///     The exercise 1.
    /// </summary>
    internal class Exercise1 : IDisposable
    {
        /// <summary>
        ///     The bytes limit can be store inside memory.
        /// </summary>
        private const int MEMORYLIMIT = 32;

        /// <summary>
        ///     The memwriter write to memStream.
        /// </summary>
        private readonly BinaryWriter memWriter;

        /// <summary>
        ///     The memory stream.
        /// </summary>
        private readonly MemoryStream memStream;

        /// <summary>
        ///     The input file stream.
        /// </summary>
        private readonly FileStream inputFileStream;

        /// <summary>
        ///     The output file stream.
        /// </summary>
        private readonly FileStream outputFileStream;

        /// <summary>
        ///     The disposed indicate if obj is already finalized.
        /// </summary>
        private bool disposed;

        /// <summary>
        /// Initializes a new instance of the <see cref="Exercise1"/> class.
        /// </summary>
        /// <param name="inputPath">
        /// The input path.
        /// </param>
        /// <param name="outputPath">
        /// The output path.
        /// </param>
        public Exercise1(string inputPath, string outputPath)
        {
            try
            {
                this.inputFileStream = new FileStream(inputPath, FileMode.Open);
                this.outputFileStream = new FileStream(outputPath, FileMode.Create);
                var byteArr = new byte[MEMORYLIMIT];
                this.memStream = new MemoryStream(byteArr, 0, MEMORYLIMIT, true, true);
                this.memWriter = new BinaryWriter(this.memStream);
            }
            catch (Exception e)
            {
                // TODO: check exception type
                Console.WriteLine(e.Message);
                throw;
            }
        }

        /// <summary>
        /// Finalizes an instance of the <see cref="Exercise1"/> class.
        /// </summary>
        ~Exercise1()
        {
            // Finalizing process called automatically by GC
            this.Cleanup(false);
        }

        /// <summary>
        ///     Read from file and write to memStream
        /// </summary>
        public void Read()
        {
            byte[] data = new byte[this.inputFileStream.Length];
            this.inputFileStream.Read(data, 0, data.Length);
            try
            {
                this.memWriter.Write(data, 0, data.Length > MEMORYLIMIT ? MEMORYLIMIT : data.Length);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                throw;
            }
        }

        /// <summary>
        ///     Write data from memStream to file
        /// </summary>
        public void Write()
        {
            try
            {
                this.memStream.WriteTo(this.outputFileStream);
            }
            catch (Exception e)
            {
                // TODO: check exception type
                Console.WriteLine(e.Message);
                throw;
            }
        }

        /// <summary>
        /// The dispose.
        /// </summary>
        public void Dispose()
        {
            this.Cleanup(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// The cleanup.
        /// </summary>
        /// <param name="disposing">
        /// indicate where the method is called from.
        /// </param>
        protected virtual void Cleanup(bool disposing)
        {
            if (this.disposed)
            {
                return;
            }

            if (disposing)
            {
                // Free managed objects
                Console.WriteLine("disposing ...");
            }

            // Free unmanaged objectsthis.memWriter.Dispose();
            this.memStream.Dispose();
            this.inputFileStream.Dispose();
            this.outputFileStream.Dispose();
            this.disposed = true;
        }
    }
}
