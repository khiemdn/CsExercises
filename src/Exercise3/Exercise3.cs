﻿// ***********************************************************************
// Assembly         : Exercise3
// Author           : KhiemDN
// Created          : 09-05-2017
// Last Modified By : Khiem
// Last Modified On : 09-06-2017  2:37 PM
// ***********************************************************************
// <copyright file="Exercise3.cs" company="VNG Corporation">
//   Copyright (c) 2017 VNG Corporation. All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************

namespace Exercise3
{
    using System;
    using System.IO;
    using System.Linq;
    using System.Net;
    using System.Text.RegularExpressions;

    /// <summary>
    ///     The exercise 3.
    /// </summary>
    public class Exercise3 : IDisposable
    {
        /// <summary>
        ///     The web client.
        /// </summary>
        private readonly WebClient webClient;

        /// <summary>
        ///     The disposed.
        /// </summary>
        private bool disposed;

        /// <summary>
        ///     The download file.
        /// </summary>
        private FileInfo file;

        /// <summary>
        ///     The file stream to write data to file.
        /// </summary>
        private FileStream fileStream;

        /// <summary>
        ///     Initializes a new instance of the <see cref="Exercise3" /> class.
        /// </summary>
        public Exercise3()
        {
            try
            {
                this.webClient = new WebClient();
                this.webClient.DownloadDataCompleted += this.DownloadComplete;
                this.webClient.DownloadProgressChanged += this.DownloadProgress;
            }
            catch (WebException e)
            {
                Console.WriteLine(e.Message);
                throw;
            }
        }

        /// <summary>
        ///     Finalizes an instance of the <see cref="Exercise3" /> class.
        /// </summary>
        ~Exercise3()
        {
            this.CleanUp(false);
        }

        /// <summary>
        ///     The download complete event.
        /// </summary>
        public event EventHandler<DownloadDataCompletedEventArgs> DownloadCompletedEvent;

        /// <summary>
        ///     The downloading event.
        /// </summary>
        public event EventHandler<DownloadProgressChangedEventArgs> DownloadingEvent;

        /// <summary>
        ///     The dispose.
        /// </summary>
        public virtual void Dispose()
        {
            this.CleanUp(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        ///     Begin downloading file in url.
        /// </summary>
        /// <param name="url">
        ///     The url.
        /// </param>
        public void Download(string url)
        {
            try
            {
                var uri = new Uri(url);
                this.file = new FileInfo(Path.GetFileName(uri.LocalPath));
                this.file.Create().Close();
                var stream = this.webClient.OpenRead(uri);
                try
                {
                    var respHeaderKeys = this.webClient.ResponseHeaders.AllKeys;
                    if (respHeaderKeys.Contains("Content-Disposition"))
                    {
                        var r = new Regex("(?<=filename=).*", RegexOptions.IgnoreCase);
                        var filename = r.Match(this.webClient.ResponseHeaders.Get("Content-Disposition")).Value;

                        if (File.Exists(filename))
                        {
                            File.Delete(filename);
                        }

                        if (!File.Exists(this.file.Name))
                        {
                            this.file.Create().Close();
                        }

                        this.file.MoveTo(filename);
                    }
                }
                finally
                {
                    stream?.Dispose();
                }

                this.webClient.DownloadDataAsync(uri);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                this.file.Delete();
                throw;
            }
        }

        /// <summary>
        ///     Cancel download.
        /// </summary>
        public void CancelDownload()
        {
            this.webClient.CancelAsync();
            this.webClient.BaseAddress = string.Empty;
        }

        /// <summary>
        ///     Clean up resources.
        /// </summary>
        /// <param name="disposing">
        ///     The disposing.
        /// </param>
        protected virtual void CleanUp(bool disposing)
        {
            if (this.disposed)
            {
                return;
            }

            if (disposing)
            {
            }

            this.webClient.Dispose();
            this.fileStream.Dispose();
            this.disposed = true;
        }

        /// <summary>
        ///     Donwload complete event.
        /// </summary>
        /// <param name="sender">
        ///     The sender.
        /// </param>
        /// <param name="e">
        ///     The e.
        /// </param>
        private void DownloadComplete(object sender, DownloadDataCompletedEventArgs e)
        {
            this.fileStream = this.file.OpenWrite();
            this.fileStream.Write(e.Result, 0, e.Result.Length);
            this.DownloadCompletedEvent?.Invoke(this, e);
        }

        /// <summary>
        ///     The download progress event.
        /// </summary>
        /// <param name="sender">
        ///     The sender.
        /// </param>
        /// <param name="e">
        ///     The DownloadProgressChangedEventArgs
        /// </param>
        private void DownloadProgress(object sender, DownloadProgressChangedEventArgs e)
        {
            this.DownloadingEvent?.Invoke(this, e);
        }
    }
}
