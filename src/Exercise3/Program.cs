﻿// ***********************************************************************
// Assembly         : Exercise3
// Author           : KhiemDN
// Created          : 09-05-2017
// Last Modified By : Khiem
// Last Modified On : 09-06-2017  10:39 AM
// ***********************************************************************
// <copyright file="Program.cs" company="VNG Corporation">
//   Copyright (c) 2017 VNG Corporation. All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************

namespace Exercise3
{
    using System;
    using System.Threading;

    internal class Program
    {
        private static void Main(string[] args)
        {
            var e3 = new Exercise3();
            e3.DownloadingEvent += (sender, e) =>
                {
                    Console.SetCursorPosition(0, 0);
                    Console.WriteLine("Downloading file progress: " + e.ProgressPercentage);
                };
            e3.DownloadCompletedEvent += (sender, e) =>
                {
                    Console.Clear();
                    Console.WriteLine("Exercise 3 \n" + "Downloading file complete, total bytes downloaded: " + e.Result.Length);
                    e3.Dispose();
                };
            while (true)
            {
                try
                {
                    Console.WriteLine("Exercise 3.\nNhap url file can download: ");
                    string url = Console.ReadLine();
                    e3.Download(url);
                    Console.Clear();
                    break;
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                }
            }

            Console.ReadLine();
        }
    }
}
