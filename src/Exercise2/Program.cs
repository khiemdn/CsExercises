﻿// ***********************************************************************
// Assembly         : Exercise2
// Author           : KhiemDN
// Created          : 09-05-2017
// Last Modified By : Khiem
// Last Modified On : 09-05-2017  3:41 PM
// ***********************************************************************
// <copyright file="Program.cs" company="VNG Corporation">
//   Copyright (c) 2017 VNG Corporation. All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************

namespace Exercise2
{
    using System;

    internal class Program
    {
        private static void Main(string[] args)
        {
            Console.WriteLine("Exercise 2");
            using (var e2 = new Exercise2("input.txt", "output.txt"))
            {
                e2.Read();
                e2.WaitingForInput();
                e2.Calculate();
                e2.WriteResult();
            }

            Console.ReadLine();
        }
    }
}
