﻿// ***********************************************************************
// Assembly         : Exercise2
// Author           : KhiemDN
// Created          : 09-05-2017
// Last Modified By : Khiem
// Last Modified On : 09-06-2017  3:07 PM
// ***********************************************************************
// <copyright file="Exercise2.cs" company="VNG Corporation">
//   Copyright (c) 2017 VNG Corporation. All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************

namespace Exercise2
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Text;

    /// <summary>
    ///     The exercise 2.
    /// </summary>
    public class Exercise2 : IDisposable
    {
        /// <summary>
        ///     The input file stream.
        /// </summary>
        private readonly FileStream inputFileStream;

        /// <summary>
        ///     The numbers.
        /// </summary>
        private readonly List<int> numbers;

        /// <summary>
        ///     The result list.
        /// </summary>
        private readonly List<int> result;

        /// <summary>
        ///     The output file stream.
        /// </summary>
        private readonly FileStream outputFileStream;

        /// <summary>
        ///     The disposed.
        /// </summary>
        private bool disposed;

        /// <summary>
        ///     Phep toan user nhap vao.
        /// </summary>
        private int inputOperation;

        /// <summary>
        ///     So user nhap vao.
        /// </summary>
        private int inputOperator;

        /// <summary>
        ///     Initializes a new instance of the <see cref="Exercise2" /> class.
        /// </summary>
        /// <param name="input">input file</param>
        /// <param name="output">output file</param>
        public Exercise2(string input, string output)
        {
            try
            {
                this.inputFileStream = new FileStream(input, FileMode.Open);
                this.outputFileStream = new FileStream(output, FileMode.Create);
                this.numbers = new List<int>();
                this.result = new List<int>();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                throw;
            }
        }

        /// <summary>
        ///     Finalizes an instance of the <see cref="Exercise2" /> class.
        /// </summary>
        ~Exercise2()
        {
            this.Cleanup(false);
        }

        /// <summary>
        ///     Read input file and save all numbers into list.
        /// </summary>
        public void Read()
        {
            var data = new byte[this.inputFileStream.Length];
            this.inputFileStream.Read(data, 0, data.Length);

            // convert to string, trim Byte order mark and ZERO WIDTH SPACE
            var dataStr = Encoding.UTF8.GetString(data).Trim(new[] { '\uFEFF', '\u200B' });

            // TODO: try lambda
            var words = dataStr.Trim().Replace(Environment.NewLine, " ").Split(' ');
            foreach (var word in words)
            {
                int result;
                if (word.Length > 0 && int.TryParse(word, out result))
                {
                    this.numbers.Add(result);
                }
            }

            string numbersOutput = string.Empty;
            this.numbers.ForEach((n) => numbersOutput += n + " ");
            Console.WriteLine("numbers found: " + numbersOutput);
        }

        /// <summary>
        ///     The waiting for input.
        /// </summary>
        public void WaitingForInput()
        {
            var isInputValid = false;
            while (!isInputValid)
            {
                Console.WriteLine("Nhap phep toan( 1.Cong 2.Tru 3.Nhan 4.Chia ): ");
                var inputStr = Console.ReadLine();
                if (int.TryParse(inputStr, out this.inputOperation))
                {
                    if (this.inputOperation >= 1 && this.inputOperation <= 4)
                    {
                        isInputValid = true;
                    }
                }
            }

            isInputValid = false;
            while (!isInputValid)
            {
                Console.WriteLine("Nhap so cua ban: ");
                var inputStr = Console.ReadLine();
                if (int.TryParse(inputStr, out this.inputOperator))
                {
                    isInputValid = true;
                }
            }
        }

        /// <summary>
        ///     Calculate the result.
        /// </summary>
        /// <returns>
        ///     The result list.
        /// </returns>
        public List<int> Calculate()
        {
            this.result.Clear();
            this.numbers.ForEach(
                (n) =>
                    {
                        switch (this.inputOperation)
                        {
                            case 1:
                                this.result.Add(n + this.inputOperator);
                                break;
                            case 2:
                                this.result.Add(n - this.inputOperator);
                                break;
                            case 3:
                                this.result.Add(n * this.inputOperator);
                                break;
                            case 4:
                                this.result.Add(n / this.inputOperator);
                                break;
                            default:
                                throw new Exception("Invalid operation");
                        }
                    });
            return this.result;
        }

        /// <summary>
        ///     The write result.
        /// </summary>
        public void WriteResult()
        {
            foreach (var i in this.result)
            {
                var str = i + " ";
                this.outputFileStream.Write(Encoding.UTF8.GetBytes(str), 0, Encoding.UTF8.GetByteCount(str));
            }

            Console.WriteLine("Writing result complete");
        }

        /// <inheritdoc />
        /// <summary>
        ///     The dispose.
        /// </summary>
        public void Dispose()
        {
            this.Cleanup(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        ///     The dispose.
        /// </summary>
        /// <param name="disposing">
        ///     The disposing.
        /// </param>
        protected virtual void Cleanup(bool disposing)
        {
            if (this.disposed)
            {
                return;
            }

            if (disposing)
            {
            }

            this.inputFileStream.Dispose();
            this.outputFileStream.Dispose();
            this.disposed = true;
        }
    }
}
