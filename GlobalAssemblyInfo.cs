﻿// ***********************************************************************
// Assembly         : Exercise1
// Author           : KhiemDN
// Created          : 09-05-2017
// Last Modified By : Khiem
// Last Modified On : 09-05-2017  10:37 AM
// ***********************************************************************
// <copyright file="GlobalAssemblyInfo.cs" company="VNG Corporation">
//   Copyright (c) 2017 VNG Corporation. All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************

using System.Reflection;
using System.Runtime.InteropServices;

[assembly: AssemblyCompany("VNG Corporation")]
[assembly: AssemblyProduct("Project")]
[assembly: AssemblyCopyright("Copyright © VNG Corporation 2017")]
[assembly: AssemblyFileVersion("1.0.0.0")]
[assembly: AssemblyVersion("1.0.0.0")]
[assembly: AssemblyInformationalVersion("1.0")]

// Setting ComVisible to false makes the types in this assembly not visible
// to COM components.  If you need to access a type in this assembly from
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

#if DEBUG
[assembly: AssemblyConfiguration("Debug")]
#else

[assembly: AssemblyConfiguration("Release")]
#endif
